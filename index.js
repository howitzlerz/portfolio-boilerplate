var express = require('express');
var app = express();
const bodyParser = require('body-parser');

//Enable templating
app.use(express.static('templates'));

// Body Parser
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get('/', function (req, res) {
     res.sendFile(__dirname + '/templates/homepage.html');
});

const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Server running on port ${port}...`));
